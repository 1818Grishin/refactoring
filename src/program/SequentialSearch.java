package program;

import java.util.Arrays;
import java.util.Scanner;
@SuppressWarnings("unused")
/**
 * Класс для поиска числа в рандомном массиве
 *
 * @author Grishin.Z.V.
 */
public class SequentialSearch {
    private static Scanner read = new Scanner(System.in);
    public static final int MIN_NUMBER_IN_ARRAY = -25;
    public static final int NUMBER_RANGE_IN_ARRAY = 51;

    public static void main(String[] args) {
        int numberIndex = -1;

        int arraySize = input("Введите кол-во ячеек массива: ");
        int[] array = new int[arraySize];
        arrayFilling(array, MIN_NUMBER_IN_ARRAY, NUMBER_RANGE_IN_ARRAY);
        System.out.println(Arrays.toString(array));
        int number = input("Введите искомое число: ");

        showNumberInArray(findNumberIndex(array, number, numberIndex));
    }

    /**
     *
     * @param message сообщение о вводе значения
     * @return введенное значение
     */
    private static int input(String message) {
        System.out.print(message);
        return read.nextInt();
    }

    /**
     * Заполняет массив рандомными числами
     * @param array массив целых чисел
     */
    private static void arrayFilling(int[] array, int MIN_NUMBER_IN_ARRAY, int NUMBER_RANGE_IN_ARRAY) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN_NUMBER_IN_ARRAY + (int) (Math.random() * NUMBER_RANGE_IN_ARRAY);
        }
    }

    /**
     * Ищет индекс искомого числа
     * @param array массив целых чисел
     * @param number искомое число
     * @param numberIndex индекс эл-та массива (имеет значение -1, если искомое число отсутствует в массиве)
     * @return ячейка массива в которой найдено число (или -1, если число отсутствует)
     */
    private static int findNumberIndex(int[] array, int number, int numberIndex) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                numberIndex = i;
                break;
            }
        }
        return numberIndex;
    }

    /**
     * Отображает индекс введенного эл-та, если такой имеется в массиве
     * @param numberIndex номер эл-та массива
     */
    private static void showNumberInArray(int numberIndex) {
        if (numberIndex == -1) {
            System.out.println("Нет такого значения");
        } else {
            System.out.println("Индекс элемента в последовательности: " + numberIndex);
        }
    }
}