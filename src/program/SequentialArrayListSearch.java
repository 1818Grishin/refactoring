package program;

import java.util.Scanner;
import java.util.ArrayList;

public class SequentialArrayListSearch {
    private static Scanner read = new Scanner(System.in);
    public static final int MIN_NUMBER_IN_ARRAY = -25;
    public static final int NUMBER_RANGE_IN_ARRAY = 51;

    public static void main(String[] args) {
        int numberIndex = -1;

        int arraySize = input("Введите кол-во ячеек массива: ");
        ArrayList<Integer> arrayList = new ArrayList(arraySize);
        arrayFilling(arrayList, MIN_NUMBER_IN_ARRAY, NUMBER_RANGE_IN_ARRAY);
        System.out.println(arrayList);
        int number = input("Введите искомое число: ");

        showNumberInArray(findNumberIndex(arrayList, number, numberIndex));
    }

    /**
     *
     * @param message сообщение о вводе значения
     * @return введенное значение
     */
    private static int input(String message) {
        System.out.print(message);
        return read.nextInt();
    }

    /**
     * Заполняет массив рандомными числами
     * @param arrayList массив целых чисел
     */
    private static void arrayFilling(ArrayList<Integer> arrayList, int MIN_NUMBER_IN_ARRAY, int NUMBER_RANGE_IN_ARRAY) {
        for (int i = 0; i < arrayList.size(); i++) {
            int temp = MIN_NUMBER_IN_ARRAY + (int) (Math.random() * NUMBER_RANGE_IN_ARRAY);
            arrayList.add(temp);
        }
    }

    /**
     * Ищет индекс искомого числа
     * @param arrayList массив целых чисел
     * @param number искомое число
     * @param numberIndex индекс эл-та массива (имеет значение -1, если искомое число отсутствует в массиве)
     * @return ячейка массива в которой найдено число (или -1, если число отсутствует)
     */
    private static int findNumberIndex(ArrayList<Integer> arrayList, int number, int numberIndex) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) == number) {
                numberIndex = i;
                break;
            }
        }
        return numberIndex;
    }

    /**
     * Отображает индекс введенного эл-та, если такой имеется в массиве
     * @param numberIndex номер эл-та массива
     */
    private static void showNumberInArray(int numberIndex) {
        if (numberIndex == -1) {
            System.out.println("Нет такого значения");
        } else {
            System.out.println("Индекс элемента в последовательности: " + numberIndex);
        }
    }
}

